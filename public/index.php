<?php

require_once('../vendor/autoload.php');

date_default_timezone_set("America/New_York");

$app = new \OneFrameLink\App();

$app->run();
