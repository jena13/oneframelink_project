<?php

namespace OneFrameLink\Controller;

// You should replace the above Namespace with your own application namespace, rather
// than the OFL namespace.

use OneFrameLink\Controller\Controller;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class MainController extends Controller
{
	public function index(ServerRequestInterface $request, ResponseInterface $response)
	{
		$template = $this->loadTemplate('Main/Show.html');
		$response->getBody()->write($template->render([]));

		return $response;	
	}
}