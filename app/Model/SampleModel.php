<?php

namespace OneFrameLink\Model;

// You should replace the above Namespace with your own application namespace, rather
// than the OFL namespace.

use OneFrameLink\DbConnection;
use OneFrameLink\Model\Model;

class SampleModel extends Model
{

	/*	
		This model extends OneFrameLink's base model, which sets up the database connection.  
		You can use any native Doctrine DBAL syntax to run queries, etc, as the base class
		simply passes through function calls that aren't defined here.
		
		See http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/ for more info.
	*/

	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function getAll()
	{
		$sql = 'SELECT * FROM test';
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		$this->data = $stmt->fetchAll();
		return $this->data;
	}
}