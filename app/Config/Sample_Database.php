<?php

return [
	'production' => [
		'db_host' => 'localhost',
		'db_user' => 'root',
		'db_password' => '',
		'db_db' => 'oneframelink',
		'db_driver' => 'pdo_mysql'
	],
	'development' => [
		'db_host' => 'localhost',
		'db_user' => 'root',
		'db_password' => '',
		'db_db' => 'oneframelink_dev',
		'db_driver' => 'pdo_mysql'
	]	
];