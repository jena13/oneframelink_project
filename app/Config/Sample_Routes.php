<?php

return [
  'routes' => [
    // if you don't want to group your routes, just put them in the '/' "group"
    // http://route.thephpleague.com/route-groups/
    // the 'path' element is relative to the group path
    '/' => [
      // matches /
      [
        'verb' => 'GET',
        'path' => '',
        'action' => 'OneFrameLink\Controller\MainController::index'
      ],
      // matches /{id}, values accessed in controllers via the $args parameter
      [
        'verb' => 'GET',
        'path' => '/{id}',
        'action' => 'OneFrameLink\Controller\MainController::show'
      ],
    ]
  ]
];