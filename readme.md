# OneFrameLink_Project

## Uses OneFrameLink
***

OneFrameLink is a tiny MVC framework for simple PHP applications.  It follows the [PSR-4](http://www.php-fig.org/psr/psr-4/) and [PSR-7](http://www.php-fig.org/psr/psr-7/) PHP-FIG specifications.  This package bootstraps an application for OFL.  To get started, edit your `composer.json` file with your project name and remove the `.git` directory so you can initialize your own repo.  Once it is installed, you can get to work in the `app/` directory.

OFL uses Doctrine DBAL for database access and Twig for Views.